package prassee

import java.net.InetSocketAddress
import com.twitter.finagle.builder.ServerBuilder
import com.twitter.finagle.http.{ Http, Request, RichHttp }

object DemoApp extends App {

  val crawlFilter = new PageCrawlFilter()
  val crawlService = new PageCrawlService()

  val socketAddress = new InetSocketAddress(8080)
  val serverName = "HTTP Crawl Endpoint"
  val server = ServerBuilder()
    .codec(new RichHttp[Request](Http()))
    .bindTo(socketAddress)
    .name(serverName)
    .build(crawlFilter.andThen(crawlService))

  println(s"microservice - ${serverName} started")
}