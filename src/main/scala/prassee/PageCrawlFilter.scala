package prassee

import com.twitter.finagle.{ Service, Filter }
import com.twitter.finagle.http.{ Response, Request }
import com.twitter.util.Future
import data.UserData
import org.jboss.netty.handler.codec.http.HttpHeaders.Names
import org.jboss.netty.handler.codec.http.HttpResponseStatus

/*
 * 
 */
class PageCrawlFilter extends Filter[Request, Response, Request, Response] {

  override def apply(request: Request,
    continue: Service[Request, Response]): Future[Response] = {
    if (request.path.startsWith("/api/")) {
      request.headers().contains(Names.AUTHORIZATION)
      continue(request)
    } else {
      unauthorized(request)
    }
  }

  def unauthorized(request: Request): Future[Response] =
    Future.value(Response(request.getProtocolVersion(), HttpResponseStatus.UNAUTHORIZED))
}