package prassee

import com.twitter.finagle.Service
import com.twitter.finagle.http.Response
import com.twitter.util.Future
import org.jboss.netty.handler.codec.http.HttpResponseStatus
import com.twitter.finagle.http.Request

/*
 * The page crawl service which reads 
 * thru the pages and saves the contents in a flat file
 */
class PageCrawlService extends Service[Request, Response] {

  def apply(request: Request): Future[Response] = {
    val response = Response(request.getProtocolVersion(), HttpResponseStatus.OK)
    response.setContentTypeJson()
    response.setContentString(s"""{
            "processed": true
          }
          """)
    Future(response)
  }

}
